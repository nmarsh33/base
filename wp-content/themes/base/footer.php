<?php global $global, $site_logo;
$footer = get_field('footer', 'option');
$footer_about = $footer['content'];
?>
</main>

<footer class="gfooter">
        <div class="gfooter__main">
            <div class="container">
                <div class="gfooter__row">
                    <div class="gfooter__content">
                    <?php if($footer_about): ?>
                        <?php echo $footer_about; ?>
                    <?php endif; ?>
                    <?php echo getSocialLinks(); ?>
                    </div>
                    <div class="gfooter__nav">
                        <nav class="menu menu--foot">
                        <span class="h5"><?php echo wp_get_nav_menu_name('foot_1') ?><?php echo showSVG('chevron'); ?></span>
                            <?php
                                wp_nav_menu(array(
                                    'container' => false,
                                    'items_wrap' => '<ul id="%1$s">%3$s</ul>',
                                    'walker' => new Base_Navwalker(),
                                    'theme_location' => 'foot_1'
                                ));
                            ?>
                        </nav>
                        <nav class="menu menu--foot">
                            <span class="h5"><?php echo wp_get_nav_menu_name('foot_1') ?><?php echo showSVG('chevron'); ?></span>
                            <?php
                                wp_nav_menu(array(
                                    'container' => false,
                                    'items_wrap' => '<ul id="%1$s">%3$s</ul>',
                                    'walker' => new Base_Navwalker(),
                                    'theme_location' => 'foot_1'
                                ));
                            ?>
                        </nav>
                        <nav class="menu menu--foot">
                            <span class="h5"><?php echo wp_get_nav_menu_name('foot_1') ?><?php echo showSVG('chevron'); ?></span>
                            <?php
                                wp_nav_menu(array(
                                    'container' => false,
                                    'items_wrap' => '<ul id="%1$s">%3$s</ul>',
                                    'walker' => new Base_Navwalker(),
                                    'theme_location' => 'foot_1'
                                ));
                            ?>
                        </nav>
                    </div>
                </div>
            </div>
        <div class="gfooter__copyright">
            <div class="container">
                <p class="copy">&copy; <?php echo date("Y"); ?> <?php bloginfo('name'); ?> | Design by <a target="_blank" href="https://nmwebdesign.net/">NM Design</a></p>
            </div>
        </div>
    </div>
</footer>

<?php wp_footer(); ?>

<?php echo get_field('body_scripts_bottom', 'option'); ?>
</body>

</html>
