<?php get_header(); ?>

<?php if( have_posts() ): ?>
   <?php while( have_posts() ): the_post(); ?>
               
		<?php get_template_part('lib/layout/flexible'); ?>

	<?php endwhile; ?>
<?php endif; ?>
<section>
	<div class="container">
		<h1>Heading 1</h1>
		<h2>Heading 2</h2>
		<h3>Heading 3</h3>
		<h4>Heading 4</h4>
		<h5>Heading 5</h5>
		<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut pharetra, risus ac malesuada scelerisque, ante ipsum porttitor felis, vitae egestas leo odio vel augue. Nulla auctor vestibulum erat egestas vestibulum. Mauris non rhoncus lorem. Suspendisse risus eros, semper ac nulla non, molestie posuere mi. Morbi eleifend, metus nec dictum aliquam, diam augue luctus nibh, a dapibus eros risus ut tellus. Donec sed venenatis nulla. Integer commodo urna sapien, pretium lobortis ante finibus ac. Etiam convallis quam quis laoreet suscipit. Proin facilisis tellus risus, ac volutpat purus venenatis in. Donec eget lacinia felis.</p>
		<ul>
			<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
			<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
			<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
			<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
			<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
		</ul>
	</div>
</section>
<section>
	<div class="container">
		<h1>Heading 1</h1>
		<h2>Heading 2</h2>
		<h3>Heading 3</h3>
		<h4>Heading 4</h4>
		<h5>Heading 5</h5>
		<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut pharetra, risus ac malesuada scelerisque, ante ipsum porttitor felis, vitae egestas leo odio vel augue. Nulla auctor vestibulum erat egestas vestibulum. Mauris non rhoncus lorem. Suspendisse risus eros, semper ac nulla non, molestie posuere mi. Morbi eleifend, metus nec dictum aliquam, diam augue luctus nibh, a dapibus eros risus ut tellus. Donec sed venenatis nulla. Integer commodo urna sapien, pretium lobortis ante finibus ac. Etiam convallis quam quis laoreet suscipit. Proin facilisis tellus risus, ac volutpat purus venenatis in. Donec eget lacinia felis.</p>
		<ul>
			<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
			<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
			<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
			<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
			<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
		</ul>
	</div>
</section>
<section>
	<div class="container">
		<h1>Heading 1</h1>
		<h2>Heading 2</h2>
		<h3>Heading 3</h3>
		<h4>Heading 4</h4>
		<h5>Heading 5</h5>
		<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut pharetra, risus ac malesuada scelerisque, ante ipsum porttitor felis, vitae egestas leo odio vel augue. Nulla auctor vestibulum erat egestas vestibulum. Mauris non rhoncus lorem. Suspendisse risus eros, semper ac nulla non, molestie posuere mi. Morbi eleifend, metus nec dictum aliquam, diam augue luctus nibh, a dapibus eros risus ut tellus. Donec sed venenatis nulla. Integer commodo urna sapien, pretium lobortis ante finibus ac. Etiam convallis quam quis laoreet suscipit. Proin facilisis tellus risus, ac volutpat purus venenatis in. Donec eget lacinia felis.</p>
		<ul>
			<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
			<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
			<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
			<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
			<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
		</ul>
	</div>
</section>
<section>
	<div class="container">
		<h1>Heading 1</h1>
		<h2>Heading 2</h2>
		<h3>Heading 3</h3>
		<h4>Heading 4</h4>
		<h5>Heading 5</h5>
		<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut pharetra, risus ac malesuada scelerisque, ante ipsum porttitor felis, vitae egestas leo odio vel augue. Nulla auctor vestibulum erat egestas vestibulum. Mauris non rhoncus lorem. Suspendisse risus eros, semper ac nulla non, molestie posuere mi. Morbi eleifend, metus nec dictum aliquam, diam augue luctus nibh, a dapibus eros risus ut tellus. Donec sed venenatis nulla. Integer commodo urna sapien, pretium lobortis ante finibus ac. Etiam convallis quam quis laoreet suscipit. Proin facilisis tellus risus, ac volutpat purus venenatis in. Donec eget lacinia felis.</p>
		<ul>
			<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
			<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
			<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
			<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
			<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
		</ul>
	</div>
</section>
<section>
	<div class="container">
		<h1>Heading 1</h1>
		<h2>Heading 2</h2>
		<h3>Heading 3</h3>
		<h4>Heading 4</h4>
		<h5>Heading 5</h5>
		<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut pharetra, risus ac malesuada scelerisque, ante ipsum porttitor felis, vitae egestas leo odio vel augue. Nulla auctor vestibulum erat egestas vestibulum. Mauris non rhoncus lorem. Suspendisse risus eros, semper ac nulla non, molestie posuere mi. Morbi eleifend, metus nec dictum aliquam, diam augue luctus nibh, a dapibus eros risus ut tellus. Donec sed venenatis nulla. Integer commodo urna sapien, pretium lobortis ante finibus ac. Etiam convallis quam quis laoreet suscipit. Proin facilisis tellus risus, ac volutpat purus venenatis in. Donec eget lacinia felis.</p>
		<ul>
			<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
			<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
			<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
			<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
			<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
		</ul>
	</div>
</section>

<?php get_footer(); ?>