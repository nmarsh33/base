<?php
    $classList = array('gheader');

    if($isHome) $classList[] = 'gheader--home';
    elseif ($isBlog) $classList[] ='gheader--blog';
    elseif ($isArchive || $isPTArchive) $classList[] ='gheader--archive';
    elseif (!empty($post_type)) $classList[] = 'gheader--'.$post_type;

    $classList[] = 'sticky';

    $class = createAttr('class', $classList);

    $header = get_field('header','option');
    $header_btn = $header['button'];
    $header_btn_target = $header_btn['target'] ? $header_btn['target'] : '_self';
    $email = $header['email'];
    $address = $header['address'];
    $google_address = $header['address_google_link'];
    $phone = $header['phone'];


?>

<header <?php echo $class; ?>>
    <!-- Top, contact info -->
    <?php if($address || $email || $phone): ?>
        <div class="gheader__top">
            <div class="container">
                <?php if($address): ?>
                 <div class="gheader__top-address">
                    <a target="_blank" href="<?php echo $google_address; ?>"><?php echo showSVG('location-pin'); ?><?php echo $address; ?></a>
                </div>
                <?php endif; ?>
                <div class="gheader__top-contact">
                     <?php if($email): ?>
                        <a href="mailto:<?php echo $email; ?>"><?php echo showSVG('email'); ?><?php echo $email; ?></a>
                    <?php endif; ?>
                     <?php if($phone): ?>
                        <a href="tel:<?php echo $phone; ?>"><?php echo showSVG('phone'); ?><?php echo $phone; ?></a>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    <?php endif; ?>

    <div class="gheader__content">
        <div class="container">

            <div class="gheader__logo">
                <a class="site-logo" href="<?php echo home_url(); ?>"><?php echo $site_logo; ?></a>
            </div>

            <nav class="global menu menu--main" aria-label="main navigation">
                <?php
                wp_nav_menu(array(
                    'container' => false,
                    'items_wrap' => '<ul id="%1$s">%3$s</ul>',
                    'walker' => new Base_Navwalker(),
                    'theme_location' => 'main'
                ));
            ?>
            </nav>

            <?php if($header_btn): ?>
                <a target="<?php echo esc_attr( $header_btn_target ); ?>" class="btn gheader__btn" href="<?php echo $header_btn['url']; ?>"><?php echo $header_btn['title']; ?></a>
            <?php endif; ?>

            <button type="button" class="menu-burger" title="Menu">
                <span class="menu-burger__icon"><span></span></span>
            </button>

        </div>

    </div>

</header>