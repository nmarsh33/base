<?php

    function createStyles($styles){
        $createStyles = '';
        
        foreach ($styles as $key => $val):
            $createStyles .= $key . ':' . $val . ';';
        endforeach;
        
        return rtrim($createStyles);
    }

    function createAttr($attr, $val = null){
        if(is_array($attr) && !empty(array_filter($attr))) {
            $attrs = $attr;
            $builtAttrs = array();

            foreach ($attrs as $key => $val) {
                if(is_array($val)) $val = join(' ', array_filter($val, 'strlen'));
                if(empty($val)) continue;

                $builtAttrs[] = $key.'="'.$val.'"';
            }

            return join(' ', array_filter($builtAttrs, 'strlen'));

        } else {
            if(is_array($val)) $val = join(' ', array_filter($val,'strlen'));
            if(empty($val)) return;

            return $attr. '="' . $val . '"';
        }
    }


	function getFile($path) {
		if (is_file($path)) {
			ob_start();
			include $path;
			return ob_get_clean();
		}
		return false;
	}

	function showSVG($name, $title = false, $icon = true){
		$svgPath = get_template_directory() . '/dist/svgs/';
		$svg = getFile($svgPath.$name.'.svg');

		if($svg){
			// title arg exists and the svg has a title tag to replace
			if( !empty($title) && preg_match("/<title>(.+)<\/title>/i", $svg, $matches)) {
				$title = '<title>'.$title.'</title>';
				$svg = preg_replace("/<title>(.+)<\/title>/i", $title, $svg);
            }

            $svgType = $icon ? 'icon' : 'code';

            $html = '<div class="svg-'.$svgType.' svg-'.$svgType.'--'.$name.'">';
            $html .= !$icon ? $svg : '<div class="positioner">'.$svg.'</div>';
            $html .= '</div>';

            return $html;
		}

		return false;
	}


    function getSocialLinks($socials = null){
        if(is_null($socials)) $socials = get_field('global', 'option')['contact']['socials'];
        
        $social_icons = array('facebook', 'instagram', 'linkedin', 'pinterest', 'tiktok', 'twitter', 'yelp', 'youtube', 'x');

        if(!empty($socials)) {
            $html = '<nav class="social-links"><ul>';

            foreach($socials as $link){
                $url = $link['url'];
                $host = parse_url($url, PHP_URL_HOST);
                $hostParts = explode('.', $host);
                $domain = $hostParts[count($hostParts) - 2];
                
                $icon = in_array($domain, $social_icons) ? $domain : 'world';
                
                $html .= '<li><a href="'.$url.'" target="_blank">';
                $html .= showSVG($icon);
                $html .= '</a></li>';
            }

            $html .= '</ul></nav>';
            return $html;
        }
        return false;
    }