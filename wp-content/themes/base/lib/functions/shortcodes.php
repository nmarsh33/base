<?php
add_shortcode('btn', 'button_shortcode');
add_shortcode('button', 'button_shortcode');
function button_shortcode($user_attr) {
	$classList = array('btn');

	$attr = shortcode_atts( array(
		'class' => '',
		'color' => '',
		'id' => '',
		'style' => '',
		'text' => '',
		'url' => '',
		'icon' => '',
		'newtab' => false
	), $user_attr);

	$styles = array_filter(explode(' ', $attr['style']));
	if($styles){
		foreach($styles as $style){
			$classList[] = 'btn--'.$style;
		}
	}

	$classList[] = $attr['class'];
	if(!empty($attr['color'])) {
		$classList[] = 'btn--'.$attr['color'];
	}

	$elTag = !empty($attr['url']) ? 'a' : 'button type="button"';

	$btn_attr = createAttr(array(
		'id' => $attr['id'],
		'href' => $attr['url'],
		'class' => $classList,
		'target' => ($elTag == 'a' && $attr['newtab'] !== false) ? '_blank' : ''
	));

	$html = '<'.$elTag.' '.$btn_attr.'>';
	if($attr['icon'] !== '') $html .= showSVG($attr['icon']);
	$html .= $attr['text'];
	$html .= '</'.$elTag.'>';

	return $html;
}

