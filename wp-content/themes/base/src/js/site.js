document.documentElement.className = document.documentElement.className.replace(/\bno-js\b/g, "") + "js"

if (window.location.hash) { setTimeout(function () { window.scrollTo(0, 0); }, 2); }

jQuery(document).ready(function ($) {


/* ========================================================================================================================

Lazy Loading

======================================================================================================================== */
    var LazyLoading = (function () {
        var instance = new LazyLoad();

        function lazyBGImages() {
            var $bgImages = $('[data-bg]:not(.lazy)');
            if ($bgImages.length) {
                $bgImages.each(function () {
                    $(this).addClass('lazy');
                });
            }
            instance.update();
        }

        function update() {
            lazyBGImages();
        }

        lazyBGImages();

        return {
            update: update
        }
    }());



/* ========================================================================================================================

Swiper Sliders

======================================================================================================================== */


/* ========================================================================================================================

Smooth Scroll

======================================================================================================================== */

var SmoothScroll = (function () {

    var $anchorLinks = $('a[href^="#"]').not('a[href="#"]');

    $('a[href="#"]').click(
        function (e) { e.preventDefault(); return; }
    );

    function goTo(target) {
        if (!target) { return; }
        if (target.startsWith('#')) {
            // Smooth scroll within the same page
            smoothScrollTo(target);
        } else {
            // Smooth scroll to a different page
            window.location.href = target;
        }
    }

    function smoothScrollTo(target) {
        var $target = $(target);
        if ($target.length) {
            var scrollPos = $target.offset().top;
            $('html, body').stop().animate({
                'scrollTop': scrollPos - 32
            }, 500, 'swing', function () {
                // Callback function if needed
            });
        }
    }

    if (window.location.hash) {
        setTimeout(function () {
            goTo(window.location.hash);
        }, 500);
    }

    if ($anchorLinks.length) {
        $anchorLinks.on('click', function (e) {
            var target = this.hash;
            e.preventDefault();
            goTo(target);
        });
    }

    return { to: goTo };

}());



/* ========================================================================================================================

Accordions

======================================================================================================================== */
var Accordions = (function () {
    var $accordions = $('.accordion');
    if (!$accordions.length) { return; }

    $accordions.each(function () {
        if ($(this).hasClass('active')) {
            $(this).find('.accordion__content').show();
        }
    });

    $accordions.find('.accordion__trigger').click(function (e) {
        var $this = $(this);
        var $accordion = $this.parent();
        var $content = $accordion.find('.accordion__content');
        var $siblings = $accordion.siblings();

        if ($accordion.hasClass('active')) {
            $accordion.removeClass('active');
            $content.slideUp('fast');
        } else {
            $accordion.addClass('active');
            $siblings.removeClass('active').find('.accordion__content').slideUp('fast');
            $content.slideDown('fast');
        }
    })

}());


 /* ========================================================================================================================

    Footer Menus

======================================================================================================================== */
    var footerMenus = (function () {
        var $navs = $('.gfooter__nav nav');
        if (!$navs.length) { return; }

        $navs.find('.h5').click(function (e) {
            var $this = $(this);
            var $navItem = $this.parent();
            var $content = $navItem.find('ul');
            var $otherNavs = $navItem.siblings('nav');

            if ($navItem.hasClass('active')) {
                $navItem.removeClass('active');
                $content.slideUp('fast');
            } else {
                $navItem.addClass('active');
                $otherNavs.removeClass('active').find('ul').slideUp('fast');
                $content.slideDown('fast');
            }
        })

    }());


/* ========================================================================================================================

Forms & Parsley

======================================================================================================================== */
    var Forms = (function () {
        var InputMasks = (function () {
            var $masks = $('[data-mask]');
            if (!$masks.length) { return; }
            var exclude_keys = [8, 13, 16, 18, 20, 27, 37, 38, 39, 40, 46];

            $('[data-mask]').keyup(function (e) {
                console.log(e.keyCode);
                if (exclude_keys.indexOf(e.keyCode) > -1) { return; }

                switch (this.dataset.mask) {
                    case 'digits':
                        var x = this.value.replace(/\D/g, '');
                        this.value = x;
                        break;
                    case 'phone':
                        var x = this.value.replace(/\D/g, '').match(/(\d{0,3})(\d{0,3})(\d{0,4})/);
                        console.log(x);
                        this.value = !x[2] ? x[1] : '(' + x[1] + ') ' + x[2] + (x[3] ? '-' + x[3] : '');
                        break;
                    case 'ssn': {
                        var x = this.value.replace(/\D/g, '').match(/^(\d{0,3})(\d{0,2})(\d{0,4})/);
                        this.value = !x[2] ? x[1] : x[1] + '-' + x[2] + '-' + x[3];
                    }
                }
            });
        }());

        var parselyOptions = {
            classHandler: function (parsleyField) {
                var $element = parsleyField.$element;
                if ($element.parent().hasClass('select-menu')) {
                    return $element.parent();
                }
                return $element;
            },
            errorsContainer: function (parsleyField) {
                var $element = parsleyField.$element;
                var $fieldContainer = $element.closest('.form-field');
                if ($fieldContainer) {
                    return $fieldContainer;
                }
            }
        };

        var formStates = (function () {
            $formInputs = $('form :input');
            if (!$formInputs.length) { return; }

            $formSelectMenus = $('.select-menu select, .ginput_container_select select');

            function isGFormInput($el) {
                return $el.parent().hasClass('ginput_container') ? $el.parent().parent() : $el;
            }

            function setFilled($input) {
                $input.addClass('filled');
            }

            function removeFilled($input) {
                $input.removeClass('filled');
            }

            function setFocused() {
                $(this).addClass('focused');
            }

            function removeFocused() {
                $(this).removeClass('focused');
            }

            function checkInput(e) {
                if (this.type == 'button' ||
                    this.type == 'range' ||
                    this.type == 'submit' ||
                    this.type == 'radio' ||
                    this.type == 'checkbox' ||
                    this.type == 'reset') { return; }

                var $this = $(this);
                var $parent = $this.parent();
                var is_selectMenu = $parent.hasClass('select-menu') || $parent.hasClass('ginput_container_select');

                var $input = is_selectMenu ? $parent : $this;

                switch (this.type) {
                    case 'select-one':
                    case 'select-multiple':
                        if (this.value !== '') {
                            setFilled($input);
                        } else {
                            removeFilled($input);
                        }
                        break;
                    default:
                        if (this.value !== '') {
                            setFilled($input);
                        } else {
                            removeFilled($input);
                        }
                        break;
                }
            }

            $formInputs.each(checkInput);
            $formInputs.on('change', checkInput);
            $formInputs.on('focus', setFocused);
            $formInputs.on('blur', removeFocused);
            $formSelectMenus.on('focus', setFocused);
            $formSelectMenus.on('blur', removeFocused);

        }());
        return { options: parselyOptions }
    }());



/* ========================================================================================================================

Header
======================================================================================================================== */
   var Header = (function () {

        var $body = $('body');

        if( $body.hasClass('child-theme') ){ return; }

        var $header = $('header.gheader');
        var $nav = $header.find('nav.global');
        var $adminBar = $('#wpadminbar');

        var header_height = $header.innerHeight();
        if ($adminBar.length) { header_height += $adminBar.innerHeight(); }
        if (window.innerWidth < 960) { $nav.css({ marginTop: header_height }); }

        var BurgerMenu = (function () {
            var $burgerMenu = $header.find('.menu-burger');

            function activate() {
                $burgerMenu.addClass('active').attr('title', 'Close');
                $nav.addClass('active');
                $body.addClass('no-scroll');

                var styles = { position: 'fixed' };
                if ($adminBar.length) { styles.top = $adminBar.innerHeight(); }

                $header.css(styles);
                $body.css({ marginTop: $header.innerHeight() });
            }

            function reset() {
                $burgerMenu.removeClass('active').attr('title', 'Menu');
                $nav.removeClass('active').find('.active').removeClass('active');
                $body.removeClass('no-scroll');

                var styles = { position: 'sticky' };
                if ($adminBar.length) { styles.top = $adminBar.innerHeight() };

                $header.css(styles);
                $body.css({ marginTop: 0 });
            }

            $burgerMenu.click(function () {
                var $this = $(this);

                if ($this.hasClass('active')) { reset(); } else { activate(); }
            });

            return {
                close: reset,
                open: activate
            }
        }());

        var DropdownMenus = (function () {
            var $menus = $('.menu');
            var $dropmenus = $menus.find('.menu-item__dropdown');
            var $mobileArrow = $dropmenus.find('.mobile-arrow');

            function toggleDropdown(e) {
                e.preventDefault();

                var $this = $(this);
                var $menuItem = $this.parent();

                if ($menuItem.hasClass('active')) {
                    $menuItem.removeClass('active');
                } else {
                    $menuItem.addClass('active');
                }
            }

            $mobileArrow.click(toggleDropdown);
        }());

        var StickyHeader = (function () {
            if (!$header.hasClass('sticky')) { return; }

            if (window.scrollY) {
                $header.addClass('sticky--scrolled');
            }

            $(window).on('scroll', function () {

                if (window.scrollY) {
                    $header.addClass('sticky--scrolled');

                } else if (window.scrollY === 0) {
                    $header.removeClass('sticky--scrolled');
                }

                if ($adminBar.length) {
                    $header.css({ top: $adminBar.innerHeight() });
                }

            });
        }());

        window.addEventListener('resize', function () {
            $header.css({ position: 'sticky' });
            BurgerMenu.close();

            var styles = { marginTop: window.innerWidth < 960 ? header_height : 0 };

            $nav.css(styles);
        });

    }());



/* ========================================================================================================================

Load More

======================================================================================================================== */

    var LoadMore = (function () {
        var $loadmore = $('#loadmore');
        if (!$loadmore.length) { return; }

        var $loadmore_text = $loadmore.text();

        $loadmore.click(function () {
            var $this = $(this);
            var $postlist = $this.siblings('.blog-posts');
            var query = WP.query;
            var page = WP.current_page;
            var max = WP.max_page;

            var data = {
                action: 'load_more_posts',
                query: query,
                page: page
            };

            $.ajax({
                url: ajaxURL,
                type: 'post',
                data: data,
                beforeSend: function () {
                    $loadmore.attr('disabled', true).text('Loading Posts');
                },
                error: function (res) {
                    res = JSON.parse(res);
                    console.log(res);
                },
                success: function (posts) {
                    if (posts) {
                        page = WP.current_page++;
                        $loadmore.attr('disabled', false).text($loadmore_text);
                        $postlist.append(posts);
                        LazyLoading.update();

                        if (WP.current_page >= max) { $loadmore.remove(); }
                    } else {
                        $loadmore.remove();
                    }
                }
            });
        });
    }());


/* ========================================================================================================================

AOS 

======================================================================================================================== */
/*  AOS.init({
        delay: 250,
        once: true,
        disable: 'mobile'
    });*/

});